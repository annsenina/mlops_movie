import src

RAW_MOVIES_DATA_PATH = "data/raw/movies.csv"
MOVIES_DATA_PATH = "data/interim/data_movies.csv"

if __name__ == "__main__":
    src.clean_data(RAW_MOVIES_DATA_PATH, MOVIES_DATA_PATH)
