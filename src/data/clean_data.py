import click
import pandas as pd


@click.command()
@click.argument('input_path', type=click.Path(exists=True))
@click.argument('output_path', type=click.Path())
def clean_data(input_path: str, output_path: str):
    """Function clean movie data
    :param input_path: Path to read original DataFrame
    :param output_path: Path to save filtered DataFrame
    :return:
    """

    movies = pd.read_csv(input_path, sep=',')
    movies.drop(movies[movies.genres == '(no genres listed)'].index,
                inplace=True)

    movies.to_csv(output_path, index=False)


if __name__ == "__main__":
    clean_data()
