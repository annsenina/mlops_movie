import re

MIN_WORDS = 4
MAX_WORDS = 200

# паттерны для удаления спецюсимволов и всяких шумов
PATTERN_S = re.compile("\'s")  # matches `'s` from text
PATTERN_RN = re.compile("\\r\\n")  # matches `\r` and `\n`
PATTERN_PUNC = re.compile(r"[^\w\s]")  # matches all non 0-9 A-z whitespace
PATTERN_HZ = re.compile("\\xa0")


def clean_text(text: str):
    """
    Series of cleaning. String to lower case, remove non words
    characters and numbers (punctuation, curly brackets etc).
    text: input text
    return: modified initial text
    """
    text = str(text).lower()  # lowercase text
    # replace the matched string with ' 'z
    text = re.sub(PATTERN_S, ' ', text)
    text = re.sub(PATTERN_RN, ' ', text)
    text = re.sub(PATTERN_PUNC, ' ', text)
    text = re.sub(PATTERN_HZ, ' ', text)
    return text
